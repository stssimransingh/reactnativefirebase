import React, { Component } from 'react';
import { View, Text,} from 'react-native';
import {Container, Content, Spinner ,Header, Item, Form, Input, Label, Button} from 'native-base';
import firebase from 'react-native-firebase';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email:'',
        password:'',
        isloading:false,
        fullname:'',
        mobile:''
    };
  }
  register(){
    if(this.state.fullname == ''){
      alert('Fullname will be required.');
    }else{

    
      this.setState({isloading:true});
      firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((data) => {
        alert(JSON.stringify(data));
        let uid = data.user.uid;
        firebase.firestore().collection('userinfo').doc(uid).set({
          fullname:this.state.fullname,
          mobile:this.state.mobile
        })
        this.setState({isloading:false});
         
      })
      .catch((err) => {
        this.setState({isloading:false});
        alert(err);
      })
    }
  }
  isspinner(){
    if(this.state.isloading){
      return(
        <Spinner></Spinner>
      )
    }
  }
  render() {
    return (
      <Container>
          <Content padder>
              <Text style={{fontSize:40, textAlign:'center'}}>Register Page</Text>
            <Form>
                <Item floatingLabel>
                    <Label>Email</Label>
                    <Input 
                        onChangeText={(text) => this.setState({email:text})}
                    />
                </Item>
                <Item floatingLabel>
                    <Label>Password</Label>
                    <Input 
                        onChangeText={(text) => this.setState({password:text})}
                    />
                </Item>
                <Item floatingLabel>
                    <Label>Full Name</Label>
                    <Input 
                        onChangeText={(text) => this.setState({fullname:text})}
                    />
                </Item>
                <Item floatingLabel>
                    <Label>Mobile</Label>
                    <Input 
                        onChangeText={(text) => this.setState({mobile:text})}
                    />
                </Item>
                <Button block success
                    disabled={this.state.isloading}
                    onPress={() => this.register()}
                >
                    {this.isspinner()}<Text>Register</Text>
                </Button>
            </Form>
          </Content>
      </Container>
    );
  }
}

export default Home;
