import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Container, Content, Spinner ,Header, Item, Form, Input, Label, Button} from 'native-base';
import firebase from 'react-native-firebase';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email:'iam@gmail.com',
        password:'123456'
    };
  }
  login(){
      firebase.auth().signInWithEmailAndPassword(this.state.email,this.state.password)
      .then(() => {
          this.props.navigation.navigate('Dash1');
      })
      .catch((err) => {
          alert(err);
      })
  }
  render() {
    return (
      <Container>
          <Content padder>
              <Text>Login</Text>
              <Form>
                  <Item floatingLabel>
                      <Label>Email</Label>
                      <Input 
                        onChangeText={(text) => this.setState({email:text})}
                        value={this.state.email}
                      />
                  </Item>
                  <Item floatingLabel>
                      <Label>Password</Label>
                      <Input 
                        onChangeText={(text) => this.setState({password:text})}
                        value={this.state.password}
                        secureTextEntry
                      />
                  </Item>
                  <Button
                  success block
                  onPress={() => this.login()}
                  >
                      <Text>Login</Text>
                  </Button>
                  <Button
                    warning
                    block
                    onPress={() => this.props.navigation.navigate('QuizTest')}
                  >
                    <Text>Take Quiz Test</Text>
                  </Button>
              </Form>
          </Content>
      </Container>
    );
  }
}

export default Login;
