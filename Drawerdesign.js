import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';
import {Icon} from 'native-base';

class Drawerdesign extends Component {
    constructor(){
        super();
        this.state = {};
    }
    getinfo(){
        let uid = firebase.auth().currentUser.uid;
        firebase.firestore().collection('userinfo').doc(uid).get()
        .then((data) => {
          this.setState({fullname:data.data().fullname});
        })
        .catch((err) => {
          alert(err)
        })
      }
      componentWillMount(){
        this.getinfo();
      }
  render() {
    return (
      <View style={{flex:1}}>
        <View style={{flex:1, backgroundColor:'red'}}>
            <View style={{alignItems:'flex-end', padding:20}}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.toggleDrawer()}
                >
                <Icon
                    name='ios-close-circle-outline'
                />
                </TouchableOpacity>
            </View>
            <Text>{this.state.fullname}</Text>
        </View>
        <View style={{flex:2}}>
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Dash')}
            >
                <Text>Dash</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Login')}
            >
                <Text>Logout</Text>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Drawerdesign;
