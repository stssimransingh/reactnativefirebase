import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Container, Content, Header} from 'native-base';
import { ProgressBar } from 'react-native-paper';
class result extends Component {
  constructor(props) {
    super(props);
    this.state = {
        correct:0,
        wrong:0,
        total:0,
        percentage:0
    };
  }
  componentWillMount(){
      let correct = this.props.navigation.getParam('correct',0);
      let wrong = this.props.navigation.getParam('wrong',0);
      let total = correct + wrong;
      let percentage = (correct / total) * 100;
      
      this.setState({correct:correct, wrong:wrong, total:total, percentage:percentage});
  }
  render() {
    return (
      <Container>
          <Header></Header>
          <Content padder>
            <View style={{padding:20, backgroundColor:'green'}}>
              <Text>Correct Answers</Text>
              <Text>{this.state.correct}</Text>
              <ProgressBar progress={this.state.percentage / 100} color="yellow" />
            </View>
            <View style={{padding:20, backgroundColor:'red'}}>
              <Text>Wrong Answers</Text>
              <Text>{this.state.wrong}</Text>
              <ProgressBar progress={1 - (this.state.percentage / 100)} color="yellow" />
            </View>
            <View style={{padding:20, backgroundColor:'yellow'}}>
              <Text>Total Answers</Text>
              <Text>{this.state.total}</Text>
              <ProgressBar progress={100} color="yellow" />
            </View>
            <View style={{padding:20, backgroundColor:'gray'}}>
              <Text>Percentage Answers</Text>
              <Text>{this.state.percentage}%</Text>
              <ProgressBar progress={this.state.percentage / 100} color="yellow" />
            </View>
          </Content>
      </Container>
    );
  }
}

export default result;
