import React, { Component } from 'react';
import { View, Text } from 'react-native';
import firebase from 'react-native-firebase';
import {Content, Container, Header, Button, Radio, Body} from 'native-base';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
class QuizTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value:0
    };
    this.arr = [];
  }
  getData(){
      firebase.firestore().collection('questions').get()
      .then((data) => {
          data.forEach((dat) => {
            this.arr.push({fbdata:dat.data(), selected:0})
          })
      })
      .then(() => {
          this.setState({abcd:''})
      })

  }
  changeValue(val, key){
    this.arr[key]['selected'] = val;
  }
  result(){
    let o;
    let correct = 0;
    let wrong = 0;
    for(o in this.arr){
      let sel = this.arr[o].selected;
      if(sel==0){
        sel = "a";
      }else if(sel==1){
        sel = "b";
      }else if(sel==2){
        sel = "c";
      }else if(sel==3){
        sel = "d";
      }
      if(this.arr[o].fbdata.answer == sel){
        correct++;
      }else{
        wrong++;
      }
    }
    this.props.navigation.navigate('result',{correct:correct, wrong:wrong});
  }
  componentWillMount(){
      this.getData();
  }
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text>Quiz Test</Text>
          </Body>
        </Header>
        <Content padder>
          {this.arr.map((item, key) => {
            let rad = [
              {label:item.fbdata.optiona, value:0},
              {label:item.fbdata.optionb, value:1},
              {label:item.fbdata.optionc, value:2},
              {label:item.fbdata.optiond, value:3},
            ]
            return(
              <View>
                <Text>Q {key + 1} : {item.fbdata.question}</Text>
                <RadioForm
                  radio_props={rad}
                  initial={item.selected}
                  buttonColor={'#2196f3'}
                  animation={true}
                  onPress={(value) => {this.changeValue(value, key)}}
                />
              </View>
            )
          })}
          <Button block success
            onPress={() => this.result()}
          >
            <Text>Submit</Text>
          </Button>
        </Content>
      </Container>
      
    );
  }
}

export default QuizTest;
