import React from 'react';
import { StyleSheet, Platform, Image, Text, View, ScrollView } from 'react-native';
import firebase from 'react-native-firebase';
import {createAppContainer, createSwitchNavigator, createMaterialTopTabNavigator, createDrawerNavigator} from 'react-navigation';
import Home from './Home';
import Login from './Login';
import Dash from './Dash';
import Drawerdesign from './Drawerdesign';
import QuizTest from './QuizTest';
import result from './result';
class App extends React.Component {
  constructor() {
    super();
    this.state = {};
  }
  createUser(){
    firebase.auth().createUserWithEmailAndPassword('simran@gmail.com','123456')
    .then((data) => {
      alert('user Created' + data.user.uid);
    })
    .catch((err) => {
      alert(err);
    })
  }
  myfun(){
    let uid = "ABCD";
    firebase.firestore().collection('users').doc(uid).set({
      name:'James Bond'
    })
    .then(() => {
      alert('Success');
    })
    .catch((err) => {
      alert(err);
    })
  }
  phonelogin(){
    let munumber  = '9781333368';
  }
  getdata(){
    firebase.firestore().collection('users').get()
    .then((data) => {
      alert(JSON.stringify(data));
    })
    .catch((err) => {
      alert(err);
    })
  }
  async componentDidMount() {
    // TODO: You: Do firebase things
    // const { user } = await firebase.auth().signInAnonymously();
    // console.warn('User -> ', user.toJSON());
      // this.createUser();
    // await firebase.analytics().logEvent('foo', { bar: '123'});
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Image source={require('./assets/ReactNativeFirebase.png')} style={[styles.logo]}/>
          <Text style={styles.welcome}>
            Welcome to {'\n'} React Native Firebase
          </Text>
          <Text style={styles.instructions}>
            To get started, edit App.js
          </Text>
          {Platform.OS === 'ios' ? (
            <Text style={styles.instructions}>
              Press Cmd+R to reload,{'\n'}
              Cmd+D or shake for dev menu
            </Text>
          ) : (
            <Text style={styles.instructions}>
              Double tap R on your keyboard to reload,{'\n'}
              Cmd+M or shake for dev menu
            </Text>
          )}
          <View style={styles.modules}>
            <Text style={styles.modulesHeader}>The following Firebase modules are pre-installed:</Text>
            {firebase.admob.nativeModuleExists && <Text style={styles.module}>admob()</Text>}
            {firebase.analytics.nativeModuleExists && <Text style={styles.module}>analytics()</Text>}
            {firebase.auth.nativeModuleExists && <Text style={styles.module}>auth()</Text>}
            {firebase.config.nativeModuleExists && <Text style={styles.module}>config()</Text>}
            {firebase.crashlytics.nativeModuleExists && <Text style={styles.module}>crashlytics()</Text>}
            {firebase.database.nativeModuleExists && <Text style={styles.module}>database()</Text>}
            {firebase.firestore.nativeModuleExists && <Text style={styles.module}>firestore()</Text>}
            {firebase.functions.nativeModuleExists && <Text style={styles.module}>functions()</Text>}
            {firebase.iid.nativeModuleExists && <Text style={styles.module}>iid()</Text>}
            {firebase.links.nativeModuleExists && <Text style={styles.module}>links()</Text>}
            {firebase.messaging.nativeModuleExists && <Text style={styles.module}>messaging()</Text>}
            {firebase.notifications.nativeModuleExists && <Text style={styles.module}>notifications()</Text>}
            {firebase.perf.nativeModuleExists && <Text style={styles.module}>perf()</Text>}
            {firebase.storage.nativeModuleExists && <Text style={styles.module}>storage()</Text>}
          </View>
        </View>
      </ScrollView>
    );
  }
}
class A extends React.Component{
  render(){
    return(
      <View style={{flex:1, justifyContent:'center'}}>
        <Text>HELLO THERE</Text>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  logo: {
    height: 120,
    marginBottom: 16,
    marginTop: 64,
    padding: 10,
    width: 135,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  modules: {
    margin: 20,
  },
  modulesHeader: {
    fontSize: 16,
    marginBottom: 8,
  },
  module: {
    fontSize: 14,
    marginTop: 4,
    textAlign: 'center',
  }
});
const barnav = createMaterialTopTabNavigator({
  Login, 
  Register:{screen:Home}
})
const drawernav = createDrawerNavigator({
  Dash
},{
  contentComponent: props => <Drawerdesign {...props} />
})
const navigation = createSwitchNavigator({
  First:{screen:barnav},
  Dash1:{screen:drawernav},
  QuizTest,
  result
  
})
export default createAppContainer(navigation);
// export default QuizTest;

