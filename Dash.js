import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Button, Container, Body, Content, Header, Form, Label, Picker, Input, Item} from 'native-base';
import firebase from 'react-native-firebase';
class Dash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      question:'',
      optiona:'',
      optionb:'',
      optionc:'',
      optiond:'',
      answer:'key'
    };
  }
  addQuestion(){
    firebase.firestore().collection('questions').add(this.state)
    .then(() => {
      alert('Data Stored');
    })
    .catch((err) => {
      alert(err);
    })
  }
  componentWillMount(){
    
  }
  render() {
      
    return (
      <Container style={{backgroundColor:'#4c6fd8'}}>
        <Content style={{padding:20}}>
          <Content padder style={{backgroundColor:'white', borderRadius:10}}>
            <Text style={{fontSize:30, textAlign:'center'}}>Add Question</Text>
            <Form>
                <Input 
                  onChangeText={(t) => this.setState({question:t})}
                  value={this.state.question}
                  style={{borderWidth:1, borderColor:'gray', borderRadius:50, padding:10, paddingLeft: 20, marginTop:20}}
                  placeholder="Question"
                />
                <Input 
                  onChangeText={(t) => this.setState({optiona:t})}
                  value={this.state.optiona}
                  style={{borderWidth:1, borderColor:'gray', borderRadius:50, padding:10, paddingLeft: 20, marginTop:20}}
                  placeholder="Option A"
                />
                <Input 
                  onChangeText={(t) => this.setState({optionb:t})}
                  value={this.state.optionb}
                  style={{borderWidth:1, borderColor:'gray', borderRadius:50, padding:10, paddingLeft: 20, marginTop:20}}
                  placeholder="Option B"
                />
                <Input 
                  onChangeText={(t) => this.setState({optionc:t})}
                  value={this.state.optionc}
                  style={{borderWidth:1, borderColor:'gray', borderRadius:50, padding:10, paddingLeft: 20, marginTop:20}}
                  placeholder="Option C"
                />
                <Input 
                  onChangeText={(t) => this.setState({optiond:t})}
                  value={this.state.optiond}
                  style={{borderWidth:1, borderColor:'gray', borderRadius:50, padding:10, paddingLeft: 20, marginTop:20}}
                  placeholder="Option D"
                />
                <Picker
                  note
                  mode="dropdown"
                  style={{ flex:1 , marginTop:20}}
                  selectedValue={this.state.answer}
                  onValueChange={(t) => this.setState({answer:t})}
                >
                  <Picker.Item label="Select Correct Answer" value="key" />
                  <Picker.Item label="A" value="a" />
                  <Picker.Item label="B" value="b" />
                  <Picker.Item label="C" value="c" />
                  <Picker.Item label="D" value="d" />
                </Picker>
                <Button
                  success
                  block
                  style={{marginTop:20}}
                  onPress={() => this.addQuestion()}
                >
                  <Text>Add Question</Text>
                </Button>
                <Button block danger
                  onPress={() => this.props.navigation.openDrawer()}
                >
                  <Text>Open</Text>
                </Button>
            </Form>
          </Content>
        </Content>
      </Container>
    );
  }
}

export default Dash;
